#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2019 - 2021
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-common.inc

KERNEL_DEFCONFIG_VERSION ?= "4.4.y-cip"

SRC_URI[sha256sum] = "70d09ed7c9dcbd227e1c77964a0a912c21253cdbf6130cefe1db7cc7b30e59f0"
