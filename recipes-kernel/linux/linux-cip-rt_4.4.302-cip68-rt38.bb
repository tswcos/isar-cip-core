#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2019 - 2021
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-rt-common.inc

KERNEL_DEFCONFIG_VERSION ?= "4.4.y-cip"

SRC_URI[sha256sum] = "bbcc9af1b08a1285a9fc5ba57cba74dcc9fb6511dd74029576ca82629d04cad7"
