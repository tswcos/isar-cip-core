#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-rt-common.inc

KERNEL_DEFCONFIG_VERSION ?= "5.10.y-cip"

SRC_URI[sha256sum] = "ca42d37fa68769259a065d766ed266b7e0ac97b018febbec2ce71db7e19f968a"
