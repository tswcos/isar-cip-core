#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2019-2022
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-common.inc

KERNEL_DEFCONFIG_VERSION ?= "4.19.y-cip"

SRC_URI[sha256sum] = "899527410a0034f859a3c05aa33d877c70db0b36ef6715c370e800015ea5ff0f"
